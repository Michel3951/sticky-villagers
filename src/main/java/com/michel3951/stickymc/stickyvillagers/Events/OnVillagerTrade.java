package com.michel3951.stickymc.stickyvillagers.Events;

import com.michel3951.stickymc.stickyvillagers.Database.Sql;
import com.michel3951.stickymc.stickyvillagers.Support.Chat;
import com.michel3951.stickymc.stickyvillagers.Support.Logger;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.MerchantInventory;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

public class OnVillagerTrade implements Listener {

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) throws SQLException, ParseException {

        Inventory inv = e.getClickedInventory();
        if (!(inv instanceof MerchantInventory)) return;

        if (!e.getCurrentItem().getType().equals(Material.ENCHANTED_BOOK)) return;

        ItemStack item = e.getCurrentItem();
        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();

        if (!meta.hasStoredEnchants()) return;

        if (!(e.getWhoClicked() instanceof Player)) return;

        Player p = (Player) e.getWhoClicked();


        if (e.getClick().isShiftClick()) {
            e.setCancelled(true);
            p.sendMessage(Chat.colored("&cShift clicking is disabled for enchanted books."));
            return;
        }

        Enchantment en = meta.getStoredEnchants().entrySet().iterator().next().getKey();

        Connection conn = null;
        ResultSet result = null;
        PreparedStatement stmt = null;

        try {
            conn = Sql.getConnection();

            String query = "SELECT * FROM `trades` WHERE `uuid` = ? AND `enchantment` = ? LIMIT 1";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getUniqueId().toString());
            stmt.setString(2, en.getKey().getKey());

            result = stmt.executeQuery();

            if (!result.next()) {
                this.updateTradingTime(p,en, conn);
                p.sendMessage(Chat.colored("&aTrade successful! Due to economy stability you can only trade the same enchantment once every 24 hours."));
                return;
            }

            String time = result.getString("most_recent_trade");
            Date now = new Date();
            Date recent = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).parse(time);

            if ((now.getTime() - recent.getTime()) <= 86400000) {
                e.setCancelled(true);
                p.sendMessage(Chat.colored("&cYou can only trade the same enchantment once every 24 hours!"));
                return;
            } else {
                p.sendMessage(Chat.colored("&aTrade successful! Due to economy stability you can only trade the same enchantment once every 24 hours."));
            }

            this.updateTradingTime(p, en, conn);
            this.saveTrade(p, en, conn);

        } catch (SQLException | ParseException ex) {
            throw ex;
        } finally {
            if (conn != null) conn.close();
        }

    }

    private void updateTradingTime(Player p, Enchantment e, Connection conn) {
        try {
            String query = "INSERT INTO `trades` (`uuid`, `enchantment`) VALUES (?, ?) ON CONFLICT(`uuid`, `enchantment`) DO UPDATE SET `most_recent_trade` = ?;";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getUniqueId().toString());
            stmt.setString(2, e.getKey().getKey());
            stmt.setString(3, (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }
    }

    private void saveTrade(Player p, Enchantment e, Connection conn) {
        try {
            String query = "INSERT INTO `trade_history` (`name`, `enchantment`) VALUES (?, ?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, p.getName());
            stmt.setString(2, e.getKey().getKey());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }
    }
}
